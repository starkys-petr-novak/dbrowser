package com.ataccama.dbrowser.core.logic.service;

import com.ataccama.dbrowser.core.DbrowserTest;
import com.ataccama.dbrowser.core.db.model.ConnectionInfo;
import com.ataccama.dbrowser.core.db.repository.ConnectionInfoRepository;
import com.ataccama.dbrowser.core.rest.model.Connection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

class ConnectionServiceTest extends DbrowserTest {

    @Autowired
    private ConnectionInfoRepository connectionInfoRepository;

    @Autowired
    private ConnectionService connectionService;

    @BeforeEach
    public void init() {
        for (int i = 0; i < CONNECTION_COUNT; i++) {
            ConnectionInfo connectionInfo = CONNECTION_INFO_BUILDER
                    .name(NAME + i)
                    .build();
            connectionInfoRepository.saveAndFlush(connectionInfo);
        }
    }

    @Test
    void listConnections() {
        List<Connection> connections = connectionService.listConnections();
        Assertions.assertEquals(CONNECTION_COUNT, connections.size());
    }

    @Test
    void saveConnection() {
        Connection actual = connectionService.saveConnection(CONNECTION_BUILDER.id(1L).build());
        Assertions.assertNotNull(actual);
        Assertions.assertNotEquals(PASSWORD, actual.getPassword());
    }

    @Test
    void deleteConnection() {
        List<Connection> beforeDeleteList = connectionService.listConnections();
        Assertions.assertEquals(CONNECTION_COUNT, beforeDeleteList.size());

        connectionService.deleteConnection(1L);

        List<Connection> afterDeleteList = connectionService.listConnections();
        Assertions.assertEquals(CONNECTION_COUNT - 1, afterDeleteList.size());
    }

    @Test
    void getConnection() {
        Connection expected = CONNECTION_BUILDER.id(1L).name(NAME + 0).build();
        Connection actual = connectionService.getConnection(1L);
        Assertions.assertNotNull(actual);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void updateConnection() {
        Connection testConnection = connectionService.getConnection(1L);
        testConnection.setHost("localhost");
        testConnection.setPort(3306);
        testConnection.setDbName("testDatabase");
        testConnection.setUsername("root");

        Connection updatedConnection = connectionService.updateConnection(testConnection);
        Assertions.assertNotNull(updatedConnection);
        Assertions.assertEquals(testConnection.getHost(), updatedConnection.getHost());
        Assertions.assertEquals(testConnection.getPort(), updatedConnection.getPort());
        Assertions.assertEquals(testConnection.getDbName(), updatedConnection.getDbName());
        Assertions.assertEquals(testConnection.getUsername(), updatedConnection.getUsername());
    }
}