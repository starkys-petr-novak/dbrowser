package com.ataccama.dbrowser.core.logic.service;

import com.ataccama.dbrowser.core.DbrowserTest;
import com.ataccama.dbrowser.core.db.model.external.mysql.Column;
import com.ataccama.dbrowser.core.db.model.external.mysql.DataRow;
import com.ataccama.dbrowser.core.db.model.external.mysql.Schema;
import com.ataccama.dbrowser.core.db.model.external.mysql.Table;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.ColumnRowMapper;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.DataRowMapper;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.SchemaRowMapper;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.TableRowMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;

class InformationServiceTest extends DbrowserTest {

    private static final String EXPECTED_SCHEMA_QUERY = "SELECT CATALOG_NAME, SCHEMA_NAME, DEFAULT_CHARACTER_SET_NAME, " +
            "DEFAULT_COLLATION_NAME FROM `information_schema`.`schemata`";
    private static final String EXPECTED_SCHEMA_ERROR_MESSAGE = "Could not retrieve schemas from database with id {1}";

    private static final String EXPECTED_TABLES_QUERY = "select table_schema, table_name, table_type, engine, version, " +
            "row_format, table_rows, avg_row_length, data_length, max_data_length, index_length, data_free, auto_increment, " +
            "create_time, update_time, check_time, table_collation, checksum, create_options, table_comment " +
            "from information_schema.tables where table_schema like 'foo'";
    private static final String EXPECTED_TABLES_ERROR_MESSAGE = "Could not retrieve tables from schema with name {foo}";

    private static final String EXPECTED_COLUMNS_QUERY = "describe `foo`.`bar`";
    private static final String EXPECTED_COLUMNS_ERROR_MESSAGE = "Could not retrieve columns from table with name {bar}";

    private static final String EXPECTED_DATA_PREVIEW_QUERY = "select * from `foo`.`bar` limit 100";
    private static final String EXPECTED_DATA_PREVIEW_ERROR_MESSAGE = "Could not retrieve data from table with name {bar}";

    // partially mocked service class
    @Mock
    private InformationService informationService;

    @BeforeEach
    public void initMocks() {
        Mockito.when(informationService.getSchemas(any())).thenCallRealMethod();
        Mockito.when(informationService.getTables(any(), any())).thenCallRealMethod();
        Mockito.when(informationService.getColumns(any(), any(), any())).thenCallRealMethod();
        Mockito.when(informationService.getDataPreview(any(), any(), any())).thenCallRealMethod();

        Mockito.when(informationService.queryData(eq(1L), eq(EXPECTED_SCHEMA_QUERY), any(SchemaRowMapper.class), eq(EXPECTED_SCHEMA_ERROR_MESSAGE)))
                .thenReturn(Arrays.asList(
                        new Schema("catalogName", "schemaName", "UTF-8", "non"),
                        new Schema("catalogName", "schemaName", "UTF-8", "non"),
                        new Schema("catalogName", "schemaName", "UTF-8", "non"),
                        new Schema("catalogName", "schemaName", "UTF-8", "non")
                ));

        Mockito.when(informationService.queryData(eq(1L), eq(EXPECTED_TABLES_QUERY), any(TableRowMapper.class), eq(EXPECTED_TABLES_ERROR_MESSAGE)))
                .thenReturn(Arrays.asList(
                        new Table("tableSchema", "tableName", "", "", "", "",
                                "", "", "", "", "", "", "",
                                "", "", "", "", "", "", ""),
                        new Table("tableSchema", "tableName", "", "", "", "",
                                "", "", "", "", "", "", "",
                                "", "", "", "", "", "", ""),
                        new Table("tableSchema", "tableName", "", "", "", "",
                                "", "", "", "", "", "", "",
                                "", "", "", "", "", "", ""),
                        new Table("tableSchema", "tableName", "", "", "", "",
                                "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "")
                ));

        Mockito.when(informationService.queryData(eq(1L), eq(EXPECTED_COLUMNS_QUERY), any(ColumnRowMapper.class), eq(EXPECTED_COLUMNS_ERROR_MESSAGE)))
                .thenReturn(Arrays.asList(
                        new Column("columnField", "columnType", "non", "key", "val", "extra"),
                        new Column("columnField", "columnType", "non", "key", "val", "extra"),
                        new Column("columnField", "columnType", "non", "key", "val", "extra"),
                        new Column("columnField", "columnType", "non", "key", "val", "extra")
                ));

        Mockito.when(informationService.queryData(eq(1L), eq(EXPECTED_DATA_PREVIEW_QUERY), any(DataRowMapper.class), eq(EXPECTED_DATA_PREVIEW_ERROR_MESSAGE)))
                .thenReturn(Arrays.asList(
                        new DataRow(Collections.singletonMap("key", "val")),
                        new DataRow(Collections.singletonMap("key", "val")),
                        new DataRow(Collections.singletonMap("key", "val")),
                        new DataRow(Collections.singletonMap("key", "val"))
                ));
    }

    @Test
    void getSchemas() {
        List<Schema> schemaList = informationService.getSchemas(1L);
        Assertions.assertEquals(4, schemaList.size());
    }

    @Test
    void getTables() {
        List<Table> tableList = informationService.getTables(1L, "foo");
        Assertions.assertEquals(4, tableList.size());
    }

    @Test
    void getColumns() {
        List<Column> tableList = informationService.getColumns(1L, "foo", "bar");
        Assertions.assertEquals(4, tableList.size());
    }

    @Test
    void getDataPreview() {
        List<DataRow> tableList = informationService.getDataPreview(1L, "foo", "bar");
        Assertions.assertEquals(4, tableList.size());
    }
}