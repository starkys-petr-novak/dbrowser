package com.ataccama.dbrowser.core;

import com.ataccama.dbrowser.DbrowserApplication;
import com.ataccama.dbrowser.configuration.DBrowserConfiguration;
import com.ataccama.dbrowser.core.db.model.ConnectionInfo;
import com.ataccama.dbrowser.core.rest.model.Connection;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = {
        DbrowserApplication.class,
        DBrowserConfiguration.class
})
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DbrowserTest {

    protected static final String DB_NAME = "testDbName";
    protected static final String HOST = "127.0.0.1";
    protected static final String NAME = "testName";
    protected static final String USERNAME = "testUser";
    protected static final String PASSWORD = "testPassword";
    protected static final int PORT = 20000;
    protected static final int CONNECTION_COUNT = 5;

    protected static final ConnectionInfo.ConnectionInfoBuilder CONNECTION_INFO_BUILDER =
            ConnectionInfo.builder()
                    .dbName(DB_NAME)
                    .host(HOST)
                    .name(NAME)
                    .username(USERNAME)
                    .password(PASSWORD)
                    .port(PORT);

    protected static final Connection.ConnectionBuilder CONNECTION_BUILDER =
            Connection.builder()
                    .dbName(DB_NAME)
                    .host(HOST)
                    .name(NAME)
                    .username(USERNAME)
                    .password(PASSWORD)
                    .port(PORT);
}
