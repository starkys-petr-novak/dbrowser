package com.ataccama.dbrowser;

import com.ataccama.dbrowser.configuration.DBrowserConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(DBrowserConfiguration.class)
public class DbrowserApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbrowserApplication.class, args);
    }

}
