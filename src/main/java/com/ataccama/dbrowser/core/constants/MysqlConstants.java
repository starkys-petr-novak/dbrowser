package com.ataccama.dbrowser.core.constants;

public final class MysqlConstants {

    private MysqlConstants() {
    }

    public static final String GET_SCHEMAS = "SELECT CATALOG_NAME, SCHEMA_NAME, DEFAULT_CHARACTER_SET_NAME, " +
            "DEFAULT_COLLATION_NAME FROM `information_schema`.`schemata`";
    public static final String GET_TABLES =
            "select " +
                    "table_schema, table_name, table_type, engine, version, row_format, table_rows, " +
                    "avg_row_length, data_length, max_data_length, index_length, data_free, auto_increment, " +
                    "create_time, update_time, check_time, table_collation, checksum, create_options, table_comment " +
                    "from information_schema.tables where table_schema like '%s'";
    public static final String GET_COLUMNS = "describe `%s`.`%s`";
    public static final String GET_DATA_PREVIEW = "select * from `%s`.`%s` limit 100";
}
