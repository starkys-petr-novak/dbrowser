package com.ataccama.dbrowser.core.constants;

public final class PackageNames {

    private PackageNames() {
    }

    public static final String DB_REPOSITORY_PATH = "com.ataccama.dbrowser.core.db.repository";
    public static final String DB_ENTITY_PATH = "com.ataccama.dbrowser.core.db.model";
    public static final String DB_COMPONENT_PATH = "com.ataccama.dbrowser.core.db";
}
