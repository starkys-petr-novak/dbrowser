package com.ataccama.dbrowser.core.rest.controller;

import com.ataccama.dbrowser.core.db.model.external.mysql.Column;
import com.ataccama.dbrowser.core.db.model.external.mysql.DataRow;
import com.ataccama.dbrowser.core.db.model.external.mysql.Schema;
import com.ataccama.dbrowser.core.db.model.external.mysql.Table;
import com.ataccama.dbrowser.core.logic.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DatabaseController {

    private final InformationService informationService;

    @Autowired
    public DatabaseController(InformationService informationService) {
        this.informationService = informationService;
    }

    @GetMapping("/connections/{id}/schemas")
    public ResponseEntity<List<Schema>> getSchemas(@PathVariable(value = "id") Long connectionId) {
        List<Schema> schemas = informationService.getSchemas(connectionId);
        return ResponseEntity.ok(schemas);
    }

    @GetMapping("/connections/{connectionId}/schema/{schemaName}")
    public ResponseEntity<List<Table>> getTables(@PathVariable(value = "connectionId") Long connectionId,
                                                 @PathVariable(value = "schemaName") String schemaName) {
        List<Table> tableList = informationService.getTables(connectionId, schemaName);
        return ResponseEntity.ok(tableList);
    }

    @GetMapping("/connections/{connectionId}/schema/{schemaName}/table/{tableName}")
    public ResponseEntity<List<Column>> getTableColumns(@PathVariable(value = "connectionId") Long connectionId,
                                                        @PathVariable(value = "schemaName") String schemaName,
                                                        @PathVariable(value = "tableName") String tableName) {
        List<Column> columns = informationService.getColumns(connectionId, schemaName, tableName);
        return ResponseEntity.ok(columns);
    }

    @GetMapping("/connections/{connectionId}/schema/{schemaName}/table/{tableName}/preview")
    public ResponseEntity<List<DataRow>> getDataPreview(@PathVariable(value = "connectionId") Long connectionId,
                                                        @PathVariable(value = "schemaName") String schemaName,
                                                        @PathVariable(value = "tableName") String tableName) {
        List<DataRow> dataRow = informationService.getDataPreview(connectionId, schemaName, tableName);
        return ResponseEntity.ok(dataRow);
    }
}
