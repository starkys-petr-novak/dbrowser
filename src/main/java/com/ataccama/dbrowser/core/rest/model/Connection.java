package com.ataccama.dbrowser.core.rest.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Connection {

    private long id;
    private String name;
    private String host;
    private Integer port;
    private String dbName;
    private String username;
    private String password;
}
