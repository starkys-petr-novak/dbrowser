package com.ataccama.dbrowser.core.rest.controller;

import com.ataccama.dbrowser.core.logic.service.ConnectionService;
import com.ataccama.dbrowser.core.rest.model.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ConnectionController {

    private final ConnectionService connectionService;

    @Autowired
    public ConnectionController(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @PostMapping("/connections")
    public ResponseEntity<Connection> createConnection(@RequestBody Connection connection) {
        Connection createdConnection = connectionService.saveConnection(connection);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdConnection);
    }

    @GetMapping("/connections")
    public ResponseEntity<List<Connection>> getAllConnections() {
        List<Connection> connections = connectionService.listConnections();
        return ResponseEntity.ok(connections);
    }

    @GetMapping("/connections/{id}")
    public ResponseEntity<Connection> getConnection(@PathVariable(value = "id") Long connectionId) {
        Connection connection = connectionService.getConnection(connectionId);
        return ResponseEntity.ok(connection);
    }

    @PutMapping("/connections")
    public ResponseEntity<Connection> updateConnection(@RequestBody Connection connection) {
        Connection updatedConnection = connectionService.updateConnection(connection);
        return ResponseEntity.ok(updatedConnection);
    }

    @DeleteMapping("/connections/{id}")
    public ResponseEntity<?> deleteConnection(@PathVariable(value = "id") Long connectionId) {
        connectionService.deleteConnection(connectionId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
