package com.ataccama.dbrowser.core.logic.service.exception;

public class ServiceException extends RuntimeException {

    public ServiceException(String message, Throwable ex) {
        super(message, ex);
    }
}
