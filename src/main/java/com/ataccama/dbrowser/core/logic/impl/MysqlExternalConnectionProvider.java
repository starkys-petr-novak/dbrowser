package com.ataccama.dbrowser.core.logic.impl;

import com.ataccama.dbrowser.core.db.model.ConnectionInfo;
import com.ataccama.dbrowser.core.db.service.InternalConnectionManager;
import com.ataccama.dbrowser.core.logic.ConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class MysqlExternalConnectionProvider implements ConnectionProvider {

    private static final String MYSQL_DRIVER_PATH = "com.mysql.cj.jdbc.Driver";
    private Map<Long, DataSource> dataSources = Collections.synchronizedMap(new HashMap<>());

    private final InternalConnectionManager internalConnectionManager;

    @Autowired
    public MysqlExternalConnectionProvider(InternalConnectionManager internalConnectionManager) {
        this.internalConnectionManager = internalConnectionManager;
    }

    @Override
    public DataSource getDataSource(Long id) {
        if (!dataSources.containsKey(id)) {
            prepareConnection(id);
        }
        return dataSources.get(id);
    }

    private void prepareConnection(Long id) {
        if (dataSources.containsKey(id)) {
            return;
        }
        ConnectionInfo connectionInfo = internalConnectionManager.get(id);
        String connectionUrl = String.format("jdbc:mysql://%s:%s/%s?useLegacyDatetimeCode=false&serverTimezone=UTC",
                connectionInfo.getHost(), connectionInfo.getPort(), connectionInfo.getDbName());
        DataSource dataSource = DataSourceBuilder.create().driverClassName(MYSQL_DRIVER_PATH)
                .url(connectionUrl)
                .username(connectionInfo.getUsername())
                .password(connectionInfo.getPassword())
                .build();
        dataSources.put(id, dataSource);
    }
}
