package com.ataccama.dbrowser.core.logic;

import javax.sql.DataSource;

public interface ConnectionProvider {

    DataSource getDataSource(Long id);
}
