package com.ataccama.dbrowser.core.logic.service;

import com.ataccama.dbrowser.core.db.model.external.mysql.Column;
import com.ataccama.dbrowser.core.db.model.external.mysql.DataRow;
import com.ataccama.dbrowser.core.db.model.external.mysql.Schema;
import com.ataccama.dbrowser.core.db.model.external.mysql.Table;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.ColumnRowMapper;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.DataRowMapper;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.SchemaRowMapper;
import com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper.TableRowMapper;
import com.ataccama.dbrowser.core.logic.impl.MysqlExternalConnectionProvider;
import com.ataccama.dbrowser.core.logic.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

import static com.ataccama.dbrowser.core.constants.MysqlConstants.*;

@Component
public class InformationService {

    private final MysqlExternalConnectionProvider connectionProvider;

    @Autowired
    public InformationService(MysqlExternalConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public List<Schema> getSchemas(Long connectionId) {
        String errorMessage = String.format("Could not retrieve schemas from database with id {%d}", connectionId);
        return queryData(connectionId, GET_SCHEMAS, new SchemaRowMapper(), errorMessage);
    }

    public List<Table> getTables(Long connectionId, String schemaName) {
        String query = String.format(GET_TABLES, schemaName);
        String errorMessage = String.format("Could not retrieve tables from schema with name {%s}", schemaName);
        return queryData(connectionId, query, new TableRowMapper(), errorMessage);
    }

    public List<Column> getColumns(Long connectionId, String schemaName, String tableName) {
        String query = String.format(GET_COLUMNS, schemaName, tableName);
        String errorMessage = String.format("Could not retrieve columns from table with name {%s}", tableName);
        return queryData(connectionId, query, new ColumnRowMapper(), errorMessage);
    }

    public List<DataRow> getDataPreview(Long connectionId, String schemaName, String tableName) {
        String query = String.format(GET_DATA_PREVIEW, schemaName, tableName);
        String errorMessage = String.format("Could not retrieve data from table with name {%s}", tableName);
        return queryData(connectionId, query, new DataRowMapper(), errorMessage);
    }

    protected <T> List<T> queryData(Long connectionId, String query, RowMapper<T> rowMapper, String errorMessage) {
        DataSource dataSource = connectionProvider.getDataSource(connectionId);
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            return jdbcTemplate.query(query, rowMapper);
        } catch (Exception e) {
            throw new ServiceException(errorMessage, e);
        }
    }
}
