package com.ataccama.dbrowser.core.logic.service;

import com.ataccama.dbrowser.core.db.model.ConnectionInfo;
import com.ataccama.dbrowser.core.db.service.InternalConnectionManager;
import com.ataccama.dbrowser.core.logic.service.exception.ServiceException;
import com.ataccama.dbrowser.core.rest.model.Connection;
import org.mindrot.jbcrypt.BCrypt;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ConnectionService {

    private final InternalConnectionManager internalConnectionManager;
    private final ModelMapper modelMapper;

    @Autowired
    public ConnectionService(InternalConnectionManager internalConnectionManager, ModelMapper modelMapper) {
        this.internalConnectionManager = internalConnectionManager;
        this.modelMapper = modelMapper;
    }

    public List<Connection> listConnections() {
        return internalConnectionManager.getAllConnections()
                .stream()
                .map(connectionInfo -> modelMapper.map(connectionInfo, Connection.class))
                .collect(Collectors.toList());
    }

    public Connection saveConnection(Connection connection) {
        ConnectionInfo connectionInfo = modelMapper.map(connection, ConnectionInfo.class);
        String hashedPassword = BCrypt.hashpw(connectionInfo.getPassword(), BCrypt.gensalt());
        connectionInfo.setPassword(hashedPassword);
        ConnectionInfo savedConnection = internalConnectionManager.save(connectionInfo);
        return modelMapper.map(savedConnection, Connection.class);
    }

    public void deleteConnection(Long id) {
        internalConnectionManager.remove(id);
    }

    public Connection getConnection(Long id) {
        ConnectionInfo connectionInfo = internalConnectionManager.get(id);
        return modelMapper.map(connectionInfo, Connection.class);
    }

    public Connection updateConnection(Connection connection) {
        ConnectionInfo savedConnection = internalConnectionManager.get(connection.getId());
        if (savedConnection == null) {
            throw new ServiceException(
                    String.format("Connection with id {%d} was not found", connection.getId()),
                    new NullPointerException());
        }
        return updateStoredConnection(savedConnection, connection);
    }

    private Connection updateStoredConnection(ConnectionInfo savedConnection, Connection connection) {
        if (connection.getName() != null &&
                !connection.getName().isEmpty() &&
                !savedConnection.getName().equals(connection.getName())) {
            savedConnection.setName(connection.getName());
        }
        if (connection.getDbName() != null && !connection.getDbName().isEmpty()) {
            savedConnection.setDbName(connection.getDbName());
        }
        if (connection.getHost() != null && !connection.getHost().isEmpty()) {
            savedConnection.setHost(connection.getHost());
        }
        if (connection.getPort() != null) {
            savedConnection.setPort(connection.getPort());
        }
        if (connection.getUsername() != null && !connection.getUsername().isEmpty()) {
            savedConnection.setUsername(connection.getUsername());
        }
        if (connection.getPassword() != null &&
                !connection.getPassword().isEmpty()) {
            String hashedNewPassword = BCrypt.hashpw(connection.getPassword(), BCrypt.gensalt());
            savedConnection.setPassword(hashedNewPassword);
        }
        return modelMapper.map(savedConnection, Connection.class);
    }
}
