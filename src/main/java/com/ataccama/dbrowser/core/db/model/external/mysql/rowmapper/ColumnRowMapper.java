package com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper;

import com.ataccama.dbrowser.core.db.model.external.mysql.Column;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ColumnRowMapper implements RowMapper<Column> {

    @Override
    public Column mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Column(
                resultSet.getString("field"),
                resultSet.getString("type"),
                resultSet.getString("null"),
                resultSet.getString("key"),
                resultSet.getString("default"),
                resultSet.getString("extra")
        );
    }
}
