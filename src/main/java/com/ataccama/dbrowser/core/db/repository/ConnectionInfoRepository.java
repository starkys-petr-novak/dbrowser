package com.ataccama.dbrowser.core.db.repository;

import com.ataccama.dbrowser.core.db.model.ConnectionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionInfoRepository extends JpaRepository<ConnectionInfo, Long> {
}
