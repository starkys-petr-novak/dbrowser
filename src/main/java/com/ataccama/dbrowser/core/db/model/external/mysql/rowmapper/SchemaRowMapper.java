package com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper;

import com.ataccama.dbrowser.core.db.model.external.mysql.Schema;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SchemaRowMapper implements RowMapper<Schema> {

    @Override
    public Schema mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Schema(
                resultSet.getString("CATALOG_NAME"),
                resultSet.getString("SCHEMA_NAME"),
                resultSet.getString("DEFAULT_CHARACTER_SET_NAME"),
                resultSet.getString("DEFAULT_COLLATION_NAME")
        );
    }
}
