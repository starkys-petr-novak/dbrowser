package com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper;

import com.ataccama.dbrowser.core.db.model.external.mysql.Table;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TableRowMapper implements RowMapper<Table> {

    @Override
    public Table mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Table(
                resultSet.getString("table_schema"),
                resultSet.getString("table_name"),
                resultSet.getString("table_type"),
                resultSet.getString("engine"),
                resultSet.getString("version"),
                resultSet.getString("row_format"),
                resultSet.getString("table_rows"),
                resultSet.getString("avg_row_length"),
                resultSet.getString("data_length"),
                resultSet.getString("max_data_length"),
                resultSet.getString("index_length"),
                resultSet.getString("data_free"),
                resultSet.getString("auto_increment"),
                resultSet.getString("create_time"),
                resultSet.getString("update_time"),
                resultSet.getString("check_time"),
                resultSet.getString("table_collation"),
                resultSet.getString("checksum"),
                resultSet.getString("create_options"),
                resultSet.getString("table_comment")
        );
    }
}
