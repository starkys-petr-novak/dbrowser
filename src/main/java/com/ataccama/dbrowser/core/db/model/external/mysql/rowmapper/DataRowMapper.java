package com.ataccama.dbrowser.core.db.model.external.mysql.rowmapper;

import com.ataccama.dbrowser.core.db.model.external.mysql.DataRow;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DataRowMapper implements RowMapper<DataRow> {

    @Override
    public DataRow mapRow(ResultSet resultSet, int index) throws SQLException {
        ResultSetMetaData md = resultSet.getMetaData();
        int columns = md.getColumnCount();
        Map<String, Object> dataRowMap = new HashMap<>(columns);
        for (int i = 1; i <= columns; ++i) {
            dataRowMap.put(md.getColumnName(i), resultSet.getObject(i));
        }
        return new DataRow(dataRowMap);
    }
}
