package com.ataccama.dbrowser.core.db.service;

import com.ataccama.dbrowser.core.db.model.ConnectionInfo;
import com.ataccama.dbrowser.core.db.repository.ConnectionInfoRepository;
import com.ataccama.dbrowser.core.logic.service.exception.NotFoundException;
import com.ataccama.dbrowser.core.logic.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class InternalConnectionManager {

    final ConnectionInfoRepository connectionInfoRepository;

    @Autowired
    public InternalConnectionManager(ConnectionInfoRepository connectionInfoRepository) {
        this.connectionInfoRepository = connectionInfoRepository;
    }

    /**
     * Returns connection from app database.
     *
     * @return connection to external database
     * @throws NotFoundException if no connection is found
     */
    public ConnectionInfo get(Long id) {
        Optional<ConnectionInfo> connectionInfo = connectionInfoRepository.findById(id);
        if (connectionInfo.isPresent()) {
            return connectionInfo.get();
        }
        throw new NotFoundException(String.format("Connection with id {%d} was not found.", id));
    }

    /**
     * Deletes connection info from the app database.
     *
     * @param id connection identifier
     * @throws ServiceException if connection with specified id was not found
     */
    public void remove(Long id) {
        try {
            connectionInfoRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(String.format("Connection with id {%d} was not found.", id));
        }
    }

    public List<ConnectionInfo> getAllConnections() {
        return connectionInfoRepository.findAll();
    }

    public ConnectionInfo save(ConnectionInfo connectionInfo) {
        try {
            return connectionInfoRepository.save(connectionInfo);
        } catch (IllegalArgumentException e) {
            throw new ServiceException("Connection could not be saved.", e);
        }
    }
}
