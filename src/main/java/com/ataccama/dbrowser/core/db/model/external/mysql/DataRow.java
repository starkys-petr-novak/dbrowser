package com.ataccama.dbrowser.core.db.model.external.mysql;

import lombok.AllArgsConstructor;

import java.util.Map;

@lombok.Data
@AllArgsConstructor
public class DataRow {

    private Map<String, Object> data;
}
