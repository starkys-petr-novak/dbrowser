package com.ataccama.dbrowser.core.db.model.external.mysql;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Column {

    private String field;
    private String type;
    private String nullable;
    private String key;
    private String defaultValue;
    private String extra;
}
