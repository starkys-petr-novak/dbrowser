# Dbrowser

### Uses:

- Maven
- Spring-Boot v2.2.2
- Spring-boot-starter-data-jpa
- Spring-boot-starter-web
- Mysql database for connections info storage
- Swagger for rest documentation (UI accesible on http://localhost:8080/swagger-ui.html )
- Jbcrypt for encrypted password storage
- ...

##### Tests
- Spring-boot starter test
- Mockito
- H2